module.exports = {
  root: true,
  env: {
    browser: true,
  },
  extends: ['plugin:vue/vue3-essential'],
  parserOptions: {
    parser: '@babel/eslint-parser',
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  plugins: ['vue'],
  rules: {},
}
