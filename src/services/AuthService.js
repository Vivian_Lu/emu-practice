import xhr from "./xhr";
import { getErrorMessage } from "@/helper/xhr.js";

class AuthService {
  getRsaKey = () => {
    return new Promise((resolve, reject) => {
      return xhr({
        method: "post",
        url: "rsa/key",
        rsa: false,
      })
        .then((res) => resolve(res))
        .catch((err) => {
          let result = {
            code: err.code,
          };
          result.message = getErrorMessage(err.code);
          reject(result);
        });
    });
  };

  login = ({ body }) => {
    return new Promise((resolve, reject) => {
      let data = {};
      data.account = body.account;
      data.password = body.password;
      data.ip = body.ip;

      return xhr({
        method: "post",
        url: "login",
        rsa: false,
        data,
        isPassMaintain: true,
        isNeedToken: false,
        isRecord: false,
        apiAction: "login",
      })
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          let result = {
            code: err.code,
          };
          result.message = getErrorMessage(err.code);
          reject(result);
        });
    });
  };

  getLoginIP = () => {
    return new Promise((resolve, reject) => {
      return xhr({
        method: "get",
        url: "ping",
        isPassMaintain: true,
        isNeedToken: false,
      })
        .then((res) => {
          return resolve(res.ip);
        })
        .catch((err) => {
          let result = {
            code: err.code,
          };
          result.message = getErrorMessage(err.code);
          reject(result);
        });
    });
  };
  getMaintainInfo = ({ body }) => {
    const url = "backMaintain/check";

    const params = {
      ip: body.ip,
    };

    return new Promise((resolve, reject) => {
      return xhr({
        method: "get",
        url,
        params,
        isPassMaintain: true,
        isNeedToken: false,
      })
        .then((res) => {
          resolve(res);
        })
        .catch((err) => {
          let result = {
            code: err.code,
          };
          result.message = getErrorMessage(err.code);
          // 返回維護資料
          result.result = err.result;
          reject(result);
        });
    });
  };
}
export default new AuthService();
