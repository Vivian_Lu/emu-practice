import { computed } from "vue";

const observe = (obj) => {
  return new Proxy(obj, {
    set(target, property, value) {
      target[property] = value;
    },
  });
};

const apiSharedPoolCore = ({ method, url, params, responseFunc }) => {
  let apiSharedPool;
  if (window.apiSharedPool === undefined) {
    window.apiSharedPool = {};
  }
  apiSharedPool = window.apiSharedPool;

  const get = (key) => {
    return apiSharedPool[key];
  };

  const add = (key, val) => {
    apiSharedPool[key] = observe({
      api: val,
    });

    remove(key);
  };

  const remove = async (key) => {
    setTimeout(() => {
      delete apiSharedPool[key];
    }, 3000);
  };

  const response = computed(async () => {
    const poolKey = `${url}?${JSON.stringify(params)}`;

    if (method.toLowerCase() !== "get") return responseFunc();
    else {
      let res;
      let apiPool = get(poolKey);

      if (apiPool === undefined) {
        res = responseFunc();
        add(poolKey, res);
      } else res = apiPool.api;

      return res;
    }
  });

  return {
    response,
  };
};

export const useApiSharedPool = (params) => {
  const buildCore = () => {
    return apiSharedPoolCore(params);
  };

  return buildCore();
};
