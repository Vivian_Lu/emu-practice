import { useApiSharedPool } from "@/services/useApiSharedPool";

const xhr = async ({
  url = "/",
  method = "get",
  rsa = import.meta.env.VUE_APP_ENABLE_RSA !== "false",
  testRsa = false,
  json = true,
  needStringify = true,
  fileType = null,
  fileName = "",
  data = null,
  params = null,
  apiAction = null,
  isPassMaintain = false,
  isNeedToken = true,
  isNeedContentType = true,
  isPassLogout = false,
  isRecord = true,
  cacheTimeOut = 3000,
  stableTime = undefined,
  allowCache = true,
  token = "",
}) => {
  const { response } = useApiSharedPool({
    method,
    url,
    params,
    data,
    responseFunc: () =>
      sendToAPI({
        url,
        method,
        rsa,
        testRsa,
        json,
        needStringify,
        fileType,
        fileName,
        data,
        params,
        apiAction,
        isPassMaintain,
        isNeedToken,
        isNeedContentType,
        isPassLogout,
        cacheTimeOut,
        allowCache,
        stableTime,
        isRecord,
        token,
      }),
  });
  return response.value;
};
export default xhr;
