import Vue from 'vue'

export const DEFAULT_SPECIAL_SITE_LIST = Object.freeze([
  { name: 'mg29', subdomain: '688' },
  { name: 'mg30', subdomain: 'o2p' },
  { name: 'sp07', subdomain: 'kb168' },
])

export const SPECIAL_SITE_LIST = (() => {
  // 深度複製 DEFAULT_SPECIAL_SITE_LIST
  const specialSiteList = JSON.parse(JSON.stringify(DEFAULT_SPECIAL_SITE_LIST))
  // 檢查是否在開發環境
  if (Helper.isDev === false) return specialSiteList

  // 更新 localStorage 既有設定
  const DEVELOPER_CONFIG = JSON.parse(
    window.localStorage.getItem('developerConfig')
  )
  // 從 DEVELOPER_CONFIG 中獲取 specialSiteList
  const DEVELOPER_SPECIAL_SITE_LIST =
    DEVELOPER_CONFIG?.specialSiteList ?? specialSiteList
  // DEVELOPER_SPECIAL_SITE_LIST 中與 specialSiteList 相同名稱網站的 subdomain 屬性更新到 specialSiteList 中
  for (const { name } of specialSiteList.values()) {
    const index = DEVELOPER_SPECIAL_SITE_LIST.findIndex(
      (item) => item.name === name
    )
    if (index === -1) continue
    specialSiteList[index].subdomain =
      DEVELOPER_SPECIAL_SITE_LIST[index].subdomain
  }
  return specialSiteList
})()

// 根據當前瀏覽器視窗的子網域，初始化網站的元信息，再根據 name 的值來設置 brand
export const SITE_META = (() => {
  const name = (() => {
    function isSubdomain(subdomain) {
      return window.location.host.startsWith(subdomain + '.')
    }

    return SPECIAL_SITE_LIST.find((item) => isSubdomain(item.subdomain))?.name
  })()

  let brand = 'mega7'
  if (name === 'mg29') {
    brand = 'gameone'
  }

  return Vue.observable({
    name,
    brand,
  })
})()

export const MENU_LIST = [
  {
    name: 'accounts',
    title: '帳號管理',
    child: [
      { name: 'hall-sub', title: '站長子帳號', route: 'accounts/hall-sub' },
    ],
  },
]
