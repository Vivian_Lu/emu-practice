import i18next from "i18next";

import config from "@/../scripts/config";
import preTranslation from "@/i18next/resources/pre-translation";
import { SITE_META } from "@/configs/site";

/**
 * @param {string} path
 * @param {import('i18next').TOptions | { preTranslationNs: string }} options https://www.i18next.com/overview/api#t
 */
export default function translate(path, options) {
  if (["", undefined, null].includes(path)) return path;

  options = options || {};
  options.interpolation = options.interpolation || {};
  options.interpolation.escapeValue =
    options.interpolation.escapeValue || false;
  options.keySeparator = options.keySeparator || "\u0000";
  options.context = options.context || SITE_META.name;

  if (options.preTranslationNs) {
    path =
      // 動態屬性訪問
      // 在 preTranslation 的物件中，從指定的 options.preTranslationNs 中提取特定 path 鍵對應的翻譯內容
      preTranslation[options.preTranslationNs][path] === undefined
        ? path
        : preTranslation[options.preTranslationNs][path];
  }

  let value = i18next.t(path, options);

  // 尚無翻譯時，使用中文進行翻譯
  // 僅正式站時預設為中文翻譯
  if (value === config.emptyValue) {
    options.lng = "en-us";

    value = i18next.t(path, options);

    if (import.meta.env.VUE_APP_SHOULD_HIGHLIGHT_MISSING_VALUE === "true")
      value += `(${path})`;
  }

  return value;
}
