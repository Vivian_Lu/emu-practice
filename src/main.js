import Vue from "vue";
import App from "@/App.vue";

import router from "@/router";
import Components from "@/components";
import store from "@/store";
import ElementUI from "element-ui";
import Vuelidate from "vuelidate";

import "element-ui/lib/theme-chalk/index.css";
import "@mdi/font/css/materialdesignicons.min.css";

Vue.config.productionTip = false;
Vue.use(Components);
Vue.use(ElementUI);
Vue.use(Vuelidate);

new Vue({
  render: (h) => h(App),

  router,
  store,
}).$mount("#app");
