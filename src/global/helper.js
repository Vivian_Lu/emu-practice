export const projectStage = (() => {
  if (/^tag:/.test(process.env.npm_package_version)) return "dev";
  if (/^qa\d+\.\d+\.\d+/.test(process.env.npm_package_version)) return "qa";
  if (/^v\d+\.\d+\.\d+/.test(process.env.npm_package_version)) return "master";
  // 保險起見，其他一律視為未知
  return "unknown";
})();

// 判斷應用程式是否處於開發環境
export const isDev =
  ["dev", "qa"].includes(projectStage) ||
  (location.protocol === "https:" &&
    [
      "m7cny.354785.com",
      "m7vnd.354785.com",
      "m7thb.354785.com",
      "m7myr.354785.com",
      "m7usd.354785.com",
    ].includes(location.hostname));

export const isType = (val, ...args) => {
  let typeName = Object.prototype.toString.call(val).slice(8, -1);
  return args.includes(typeName);
};
