import InputText from "./input/InputText.vue";
import BaseButton from "./button/BaseButton.vue";

export default {
  install(Vue) {
    Vue.component("InputText", InputText);
    Vue.component("BaseButton", BaseButton);
  },
};
