// src/store/index.js
import Vue from "vue";
import Vuex from "vuex";
import AuthService from "@/services/AuthService";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    apiToken: null,
    account: "",
    status: 0,
    roleId: 0,
    pageCode: "",
    menu: [],
    publicKey: "",
    loginIP: "0.0.0.0",
  },

  getters: {},

  mutations: {
    SET_DEFAULT(state) {
      window.localStorage.removeItem("vuex");

      state.apiToken = "";
      state.account = "";
      // -1維護, 0未登入, 1登入
      state.status = 0;
      state.roleId = 0;
      state.pageCode = "";
      state.menu.length = 0;
      state.publicKey = "";
    },
    SET_LOGIN_IP(state, payload) {
      state.loginIP = payload;
    },
    SET_STATUS(state, payload) {
      state.status = payload;
    },
  },

  actions: {
    setDefault({ commit }) {
      commit("SET_DEFAULT");
    },
    setMaintain({ commit }) {
      commit("SET_STATUS", -1);
    },
    checkMaintain({ state }) {
      return new Promise((resolve, reject) => {
        AuthService.getMaintainInfo({ body: { ip: state.loginIP } })
          .then(() => {
            return resolve();
          })
          .catch((err) => {
            return reject(err);
          });
      });
    },
    login({ dispatch, commit }, param) {
      window.localStorage.setItem("isRemember", param.body.isRemember);

      // const asyncRsaKey = AuthService.getRsaKey()

      return new Promise((resolve, reject) => {
        AuthService.login({ body: param.body })
          .then(async (res) => {
            let account = param.body.account;
            window.localStorage.setItem("account", account);
            commit("SET_ACCOUNT", account);
            commit("SET_API_TOKEN", res.api_token);
            // const asyncRsaKey = AuthService.getRsaKey();
            // const rsaKey = await asyncRsaKey;
            // let key = window.atob(rsaKey.Key);

            // commit("SET_ROLE_ID", res.role_id);
            // commit("SET_IS_MASTER", res.is_master);
            // commit("SET_HALL_CODE", res.hall_code);
            // commit("SET_HAS_DEFAULT_AGENT", res.has_default_agent);
            //   let siteInfo = {
            //     code: "",
            //     name: "",
            //     country_id: "",
            //     customer_url: "",
            //   };
            //   if (typeof res.site_code === "object" && res.site_code !== null) {
            //     siteInfo = {
            //       code: res.site_code.site_code ? res.site_code.site_code : "",
            //       name: res.site_code.name ? res.site_code.name : "",
            //       country_id: res.site_code.country_id
            //         ? res.site_code.country_id.num()
            //         : "",
            //       customer_url: res.site_code.customer_url || "",
            //       isDirector: res.site_code.director_status !== "1",
            //     };
            //   }
            //   commit("SET_SITE_INFO", siteInfo);
            //   commit("SET_PUBLIC_KEY", key);
            //   commit("SET_STATUS", 1);
            //   const promises = [];
            //   promises.push(dispatch("setPublicKey"));
            //   promises.push(dispatch("getHallInfo"));
            //   promises.push(dispatch("getTags"));
            //   promises.push(dispatch("getHallTypeProviderList"));
            //   promises.push(dispatch("getProviderList"));
            //   promises.push(dispatch("getGameKindList"));
            //   await Promise.all(
            //     promises.map(async (promise) => {
            //       await promise.catch((error) => {
            //         App.$message.error(error.message);
            //       });
            //     })
            //   );
            return resolve();
          })
          .catch((err) => {
            return reject(err);
          });
      });
    },
    getLoginIP({ commit }) {
      return new Promise((resolve, reject) => {
        AuthService.getLoginIP()
          .then((res) => {
            commit("SET_LOGIN_IP", res);
            return resolve();
          })
          .catch((err) => {
            return reject(err);
          });
      });
    },
  },
});
