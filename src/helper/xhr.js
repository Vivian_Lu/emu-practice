import translate from '@/i18next/translate'
import i18n from 'i18next'

export const XHR_TIMEOUT = 900000
export const DEFAULT_CACHE_EXPIRY_TIME = 3000

export const getErrorMessage = (code, options) => {
  const isNumber = /^[0-9]+$/.test(code)
  const isSuccess = /^1$/.test(code)
  const language = i18n.language
  const message = translate(code, {
    preTranslationNs: '錯誤代碼',
    ns: '錯誤代碼',
    lng: language,
    ...options,
  })
  const result = isNumber && !isSuccess ? `[${code}] ${message}` : message
  return message !== undefined ? result : code
}

export const getWarningMessage = getErrorMessage
