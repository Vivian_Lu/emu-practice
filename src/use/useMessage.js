import { getCurrentInstance } from "vue";

export const useMessage = () => {
  return {
    $message: getCurrentInstance().proxy.$message,
    $confirm: getCurrentInstance().proxy.$confirm,
    $alert: getCurrentInstance().proxy.$alert,
  };
};
