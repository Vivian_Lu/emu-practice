import { getCurrentInstance, onMounted, ref } from "vue";

export const useValidation = () => {
  const $v = ref({});

  onMounted(() => {
    $v.value = getCurrentInstance().proxy.$v;
  });

  return {
    $v: $v,
  };
};
