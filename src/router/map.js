export const routes = [
  {
    path: "/",
    redirect: { name: "dashboard" },
    // component: () => import("@/pages"),
    children: [
      {
        path: "/",
        component: () => import("@/pages/dashboard/Dashboard.vue"),
        name: "dashboard",
        meta: { requiresAuth: true },
      },
      {
        path: "/accounts/hall-sub",
        component: () => import("@/pages/hall-sub/HallSub.vue"),
        name: "accounts/hall-sub",
        meta: { requiresAuth: true },
      },
    ],
  },
  {
    path: "/login",
    component: () => import("@/pages/login/Login.vue"),
    name: "login",
  },
];
