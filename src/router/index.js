import Vue from 'vue'
import Router from 'vue-router'
import { routes } from './map'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes,
})

const checkAuth = async (to, from, next) => {
  await !!router.app.$root
  if (to.meta.requiresAuth) {
    if (router.app.$store.state.status === 1) next()
    else {
      router.app.$store.dispatch('setDefault')
      next({ name: 'login' })
    }
  } else {
    next()
  }
}

router.beforeEach((to, from, next) => {
  checkAuth(to, from, next)
})

export default router
