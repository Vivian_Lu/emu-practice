const path = require('path')
const webpack = require('webpack')
const packageJSON = require('./package.json')

process.env.VUE_APP_VERSION = packageJSON.version

// 除錯時打開，並安裝套件
// const mergeDebugWebpackConfig = config => {
//   config.optimization.minimize(false)

//   config.plugin('speed-measure')
//     .use('speed-measure-webpack-plugin')

//   config.plugin('bundle-analyzer')
//     .use(require('webpack-bundle-analyzer').BundleAnalyzerPlugin)
// }

const dotenv = require('dotenv')
dotenv.config()

const mergeBaseWebpackConfig = (config) => {
  config.stats({
    colors: true,
    assets: true,
    chunks: false,
    chunkModules: false,
  })

  // 不使用 prefetch
  config.plugins.delete('prefetch')

  config.plugin('provide').use(
    new webpack.ProvidePlugin({
      Helper: '@/global/helper.js',
    })
  )

  config.module.rules.delete('svg')
  config.module
    .rule('svg')
    .test(/\.svg$/)
    .use('svg-url-loader')
    .loader('svg-url-loader')
    .options({ encoding: 'base64' })
    .end()

  config.module.rules.delete('eslint')

  config.resolve.extensions
    .add('.scss')
    .end()
    .alias.set('~', path.resolve('src/assets'))
    .end()

  // 關閉預設的 sourceMap
  // config.optimization
  //   .minimizer('terser')
  //   .tap(args => {
  //     args[0].sourceMap = false
  //     return args
  //   })
  //   .end()
}

const mergeDevelopmentWebpackConfig = (config) => {
  config.plugin('webpack-notifie').use(require('webpack-notifier'), [
    {
      contentImage: path.join(__dirname, 'public', 'favicon.png'),
    },
  ])
}

const mergeProductionWebpackConfig = (config) => {
  console.log(
    `\n` +
      `\n` +
      `        ::::::::::      :::   :::      :::    :::\n` +
      `       :+:            :+:+: :+:+:     :+:    :+:\n` +
      `      +:+           +:+ +:+:+ +:+    +:+    +:+\n` +
      `     +#++:++#      +#+  +:+  +#+    +#+    +:+\n` +
      `    +#+           +#+       +#+    +#+    +#+\n` +
      `   #+#           #+#       #+#    #+#    #+#\n` +
      `  ##########    ###       ###     ########\n` +
      `\n` +
      `\n`
  )

  config.performance.hints('warning')

  config.plugin('optimize-css').tap((args) => {
    // svgo 會使 SVG 壞掉
    args[0].cssnanoOptions.preset[1].svgo = false
    // https://github.com/cssnano/cssnano/issues/957
    args[0].cssnanoOptions.preset[1].convertValues = false
    return args
  })
}

module.exports = {
  runtimeCompiler: true,

  chainWebpack: (config) => {
    // mergeDebugWebpackConfig(config)

    mergeBaseWebpackConfig(config)

    if (process.env.NODE_ENV === 'production') {
      mergeProductionWebpackConfig(config)
    } else {
      mergeDevelopmentWebpackConfig(config)
    }
  },

  devServer: {
    host: process.env.VUE_APP_HOST_NAME,
    https: true,
    proxy: {
      '/api': {
        target: process.env.VUE_APP_API_URL,
        changeOrigin: true,
        secure: false,
        pathRewrite: {
          '^/api': '',
        },
      },
      '/issue': {
        target: process.env.VUE_APP_SLACK_API_URL,
        changeOrigin: true,
        secure: false,
        pathRewrite: {
          '^/issue': '',
        },
      },
    },
  },
}
