module.exports = {
  allLanguageTags: [
    'ms-my',
    'vi-vn',
    'th-th',
    'en-us',
    'zh-cn',
    'zh-hk',
    'zh-tw',
  ],
  requiredLanguageTagsForPages: {
    $all: ['th-th', 'en-us', 'zh-cn', 'zh-tw'],
  },
  // https://github.com/ElemeFE/element/tree/dev/src/locale/lang
  elementUiLanguageMapping: {
    'zh-cn': 'zh-CN',
    'en-us': 'en',
    'th-th': 'th',
    'vi-vn': 'vi',
    'zh-tw': 'zh-TW',
  },
  sourceFilePath: 'scripts/translation.xlsx',
  destinationDirectoryPath: 'src/i18next/resources/',
  destinationFileSuffix: '-xlsx-translation',
  destinationExtension: '.json',
  languageTagRow: 1,
  dataStartRow: 2,
  emptyValue: '$empty',

  // 'allLanguageTags': [],
  // 'requiredLanguageTagsForPages': {
  //   '[string] key: $all 表示套用至所有分頁 或 分頁名稱表示套用至特定分頁': [
  //     '[string] value: 可為任意字元 表示必須要有的欄位, 以及需要生成的翻譯檔案'
  //   ]
  // },
  // 'sourceFilePath': '[string] 表示要轉換的 excel 檔案位置, 專案根目錄的相對路徑',
  // 'destinationDirectoryPath': '[string] 表示要儲存翻譯 js 檔案的資料夾位置, 專案根目錄的相對路徑',
  // 'destinationFileSuffix': '[string] 表示要儲存翻譯 js 檔案的名稱後綴',
  // 'languageTagRow': '[int]',
  // 'dataStartRow': '[int]',
  // 'emptyValue': '$empty',
}
