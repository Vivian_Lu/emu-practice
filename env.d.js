const importMeta = {
  env: {
    VUE_APP_HOST_NAME: "",
    VUE_APP_API_URL: "",
    VUE_APP_ENABLE_RSA: "",
    VUE_APP_SHOULD_LOG_MISSING_KEY: "",
    VUE_APP_SHOULD_HIGHLIGHT_MISSING_VALUE: "",
    VUE_APP_LOG_API_ERROR: "",
    VUE_APP_IS_DEV: "",
    DEV: "",
    VUE_APP_SLACK_API_URL: "",
    VUE_APP_NEW_AUDIT_SWITCH_IS_ON: "",
    VUE_APP_LOG_GAMENAME_ERROR: "",
  },
};
export default importMeta;
